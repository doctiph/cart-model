<?php

namespace Doctipharma\CommonCartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

class CartShop
{
    protected $id;

    protected $cart;

    protected $shopId;

    protected $shopName;

    protected $price;

    protected $shippingPrice;

    protected $totalPrice;

    protected $shippingTypeCode;

    protected $shippingTypeLabel;

    protected $cartItems;

    protected $updatedAt;

    protected $createdAt;

    protected $shippingTypes;

    protected $shippingErrorCode;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartItems     = new \Doctrine\Common\Collections\ArrayCollection();
        $this->shippingTypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cart
     *
     * @param Cart $cart
     * @return CartShop
     */
    public function setCart(Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Add cartItems
     *
     * @param CartItem $cartItems
     * @return CartShop
     */
    public function addCartItem(CartItem $cartItems)
    {
        $this->cartItems[] = $cartItems;

        return $this;
    }

    /**
     * Remove cartItems
     *
     * @param CartItem $cartItems
     */
    public function removeCartItem(CartItem $cartItems)
    {
        $this->cartItems->removeElement($cartItems);
    }

    /**
     * Get cartItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }

    /**
     * Set shopId (MiraklId)
     *
     * @param integer $shopId
     * @return CartShop
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId (MiraklId)
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set shopName
     *
     * @param string $shopName
     * @return CartShop
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;

        return $this;
    }

    /**
     * Get shopName
     *
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * Check if there is items in the cartShop
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return count($this->getCartItems()) == 0;
    }

    /**
     * Set shippingTypeCode
     *
     * @param string $shippingTypeCode
     * @return CartShop
     */
    public function setShippingTypeCode($shippingTypeCode)
    {
        $this->shippingTypeCode = $shippingTypeCode;

        return $this;
    }

    /**
     * Get shippingTypeCode
     *
     * @return string
     */
    public function getShippingTypeCode()
    {
        return $this->shippingTypeCode;
    }

    /**
     * Transform cart shop into an array
     *
     * @return array
     */
    public function toArray()
    {
        $item = array(
            'id' => $this->getId(),
            'shop_id' => $this->getShopId(),
            'shop_name' => $this->getShopName(),
            'price' => $this->getPrice(),
            'shipping_price' => $this->getShippingPrice(),
            'total_price' => $this->getTotalPrice()
        );

        $item['shipping_type_code'] = $this->getShippingTypeCode();
        $item['shipping_type_label'] = $this->getShippingTypeLabel();
        $item['shipping_types'] = $this->getShippingTypes();

        if ($this->shippingErrorCode) {
            $item['error_code'] = $this->getShippingErrorCode();
        }

        foreach ($this->getCartItems() as $cartItem) {
            $item['items'][] = $cartItem->toArray();
        }

        return $item;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return CartShop
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     * @return CartShop
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Refresh price of the shop
     *
     * @param bool $cleanShipping clean the shipping price
     *
     * @return void
     */
    public function refreshPrice($cleanShipping = false)
    {
        $price = 0;
        $totalPrice = 0;
        $shippingPrice = 0;
        foreach ($this->getCartItems() as $cartItem) {
            $cartItem->refreshPrice($cleanShipping);
            $price += $cartItem->getPrice();
            $shippingPrice += $cartItem->getShippingPrice();
            $totalPrice += $cartItem->getTotalPrice();
        }

        $this->setPrice($price);
        if ($cleanShipping) {
            $this->setTotalPrice(null);
            $this->setShippingPrice(null);
            $this->setShippingTypeCode(null);
            $this->setShippingTypeLabel(null);
        } else {
            $this->setTotalPrice($totalPrice);
            $this->setShippingPrice($shippingPrice);
        }
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CartShop
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CartShop
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets Shipping types
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $shippingTypes
     *
     * @return CartShop
     */
    public function setShippingTypes($shippingTypes)
    {
        $this->shippingTypes = $shippingTypes;

        return $this;
    }

    /**
     * Gets Shipping types
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getShippingTypes()
    {
        return $this->shippingTypes;
    }

    /**
     * Sets shippingTypeLabel
     *
     * @param string $shippingTypeLabel
     *
     * @return CartShop
     */
    public function setShippingTypeLabel($shippingTypeLabel)
    {
        $this->shippingTypeLabel = $shippingTypeLabel;

        return $this;
    }

    /**
     * Gets shippingTypeLabel
     *
     * @return string
     */
    public function getShippingTypeLabel()
    {
        return $this->shippingTypeLabel;
    }

    /**
     * Get CartItem with a specific id
     *
     * @param integer $id
     *
     * @return CartItem
     */
    public function getOfferById($id)
    {
        $item = null;
        foreach ($this->getCartItems() as $cartItem) {
            if ($cartItem->getOfferId() == $id) {
                $item = $cartItem;
                break;
            }
        }

        return $item;
    }

    /**
     * @param string $shippingErrorCode
     *
     * @return CartShop
     */
    public function setShippingErrorCode($shippingErrorCode)
    {
        $this->shippingErrorCode = $shippingErrorCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getShippingErrorCode()
    {
        return $this->shippingErrorCode;
    }

    /**
     * Replace items
     *
     * @param array $newCartItems
     *
     * @return array
     */
    public function replaceItems($newCartItems)
    {
        // Manage existing cart item
        $removedItems = array();
        foreach ($this->getCartItems() as $currentCartItem) {
            $foundItem = false;

            foreach ($newCartItems as $newCartItem) {
                if ($currentCartItem->getOfferId() == $newCartItem->getOfferId()) {
                    $currentCartItem->replace($newCartItem);
                    $foundItem = true;
                }
            }

            if (! $foundItem) {
                $this->removeCartItem($currentCartItem);
                $removedItems[] = $currentCartItem;
            }
        }

        // Manage new cart item not replaced in existing
        foreach ($newCartItems as $newCartItem) {
            $foundItem = ($this->getOfferById($newCartItem->getOfferId()) instanceof CartItem);

            if (! $foundItem) {
                $this->addCartItem($newCartItem);
                $newCartItem->setShop($this);
            }
        }

        return $removedItems;
    }

    /**
     * to string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getId() . '|' . $this->getShopId();
    }

    /**
     * Set shippingPrice
     *
     * @param float $shippingPrice
     * @return CartItem
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    /**
     * Get shippingPrice
     *
     * @return float
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    public function castAs($shopClass, $itemClass)
    {
        $cartShop = new $shopClass();
        foreach (get_object_vars($this) as $key => $name) {
            $cartShop->$key = $name;
        }

        $cartItems = new ArrayCollection();

        foreach ($this->getCartItems() as $cartItem) {
            $newCartItem = $cartItem->castAs($itemClass);
            $newCartItem->setShop($cartShop);

            $cartItems[] = $newCartItem;
        }
        $cartShop->cartItems = $cartItems;

        return $cartShop;
    }

    public function getProductIds()
    {
        $ids = [];

        foreach ($this->getCartItems() as $item) {
            $ids[] = $item->getProductSku();
        }

        return $ids;
    }
}
