<?php

namespace Doctipharma\CommonCartBundle\Manager;

use Doctipharma\CommonCartBundle\Entity\Cart;
use Psr\SimpleCache\CacheInterface;

class CacheManager
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var int
     */
    protected $lifetime;

    /**
     * @param CacheInterface $cache
     * @param int            $lifetime
     */
    public function __construct(CacheInterface $cache, int $lifetime)
    {
        $this->cache = $cache;
        $this->lifetime = $lifetime;
    }

    public function fetch(string $cartId)
    {
        return $this->cache->get($cartId);
    }

    public function contains(string $cartId)
    {
        return $this->cache->has($cartId);
    }

    public function save(Cart $cart, $lifetime = null)
    {
        if ($lifetime === null) {
            $lifetime = $this->lifetime;
        }
        if (get_class($cart) !== 'Doctipharma\CommonCartBundle\Entity\Cart') {
            $cart = $cart->toCommon();
        }
        try {
            return $this->cache->set((string) $cart->getId(), $cart, $lifetime);
        } catch (\Exception $e) {
            return $this->delete($cart);
        }
    }

    public function delete(Cart $cart)
    {
        return $this->cache->delete((string) $cart->getId());
    }
}
