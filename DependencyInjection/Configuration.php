<?php

namespace Doctipharma\CommonCartBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('doctipharma_common_cart');

        $rootNode
            ->children()
                ->scalarNode('cache_client')->isRequired()->end()
                ->scalarNode('cache_lifetime')->defaultValue(0)->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
