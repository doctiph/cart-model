<?php

namespace Doctipharma\CommonCartBundle\Entity;

class OptimizedCart
{
    protected $id;

    protected $cart;

    protected $optimizedCart;

    protected $hash;

    protected $updatedAt;

    protected $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optimizedCart
     *
     * @param string $optimizedCart
     * @return OptimizedCart
     */
    public function setOptimizedCart($optimizedCart)
    {
        $this->optimizedCart = $optimizedCart;

        return $this;
    }

    /**
     * Get optimizedCart
     *
     * @return string
     */
    public function getOptimizedCart()
    {
        return $this->optimizedCart;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return OptimizedCart
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return OptimizedCart
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set cart
     *
     * @param Cart $cart
     * @return OptimizedCart
     */
    public function setCart(Cart $cart)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Gets the value of hash.
     *
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Sets the value of hash.
     *
     * @param mixed $hash the hash
     *
     * @return self
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    public function castAs($optimizedCartClass)
    {
        $optimizedCart = new $optimizedCartClass();
        foreach (get_object_vars($this) as $key => $name) {
            $optimizedCart->$key = $name;
        }

        return $optimizedCart;
    }
}
