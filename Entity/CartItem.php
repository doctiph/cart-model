<?php

namespace Doctipharma\CommonCartBundle\Entity;

class CartItem
{
    protected $id;

    protected $offerPrice;

    protected $offerId;

    protected $quantity;

    protected $originalQuantity;

    protected $initialQuantity;

    protected $availableQuantity;

    protected $price;

    protected $totalPrice;

    protected $shippingPrice;

    protected $shop;

    protected $productSku;

    protected $productTitle;

    protected $updatedAt;

    protected $createdAt;

    protected $isDrug = false;

    protected $ean;

    protected $shippingPriceUnit;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets Id
     *
     * @param int $id
     *
     * @return CartItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $ean
     */
    public function setEan($ean)
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * @return string
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * Set offerPrice
     *
     * @param float $offerPrice
     * @return CartItem
     */
    public function setOfferPrice($offerPrice)
    {
        $this->offerPrice = $offerPrice;

        return $this;
    }

    /**
     * Get offerPrice
     *
     * @return float
     */
    public function getOfferPrice()
    {
        return $this->offerPrice;
    }

    /**
     * Set offerId
     *
     * @param string $offerId
     * @return CartItem
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;

        return $this;
    }

    /**
     * Get offerId
     *
     * @return string
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return CartItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set shop
     *
     * @param CartShop $shop
     * @return CartItem
     */
    public function setShop(CartShop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return CartShop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set originalQuantity
     *
     * @param integer $originalQuantity
     * @return CartItem
     */
    public function setOriginalQuantity($originalQuantity)
    {
        $this->originalQuantity = $originalQuantity;

        return $this;
    }

    /**
     * Get originalQuantity
     *
     * @return integer
     */
    public function getOriginalQuantity()
    {
        return $this->originalQuantity;
    }

    /**
     * Set initialQuantity
     *
     * @param integer $initialQuantity
     * @return CartItem
     */
    public function setInitialQuantity($initialQuantity)
    {
        $this->initialQuantity = $initialQuantity;

        return $this;
    }

    /**
     * Get initialQuantity
     *
     * @return integer
     */
    public function getInitialQuantity()
    {
        return $this->initialQuantity;
    }

    /**
     * Set availableQuantity
     *
     * @param integer $availableQuantity
     * @return CartItem
     */
    public function setAvailableQuantity($availableQuantity)
    {
        $this->availableQuantity = $availableQuantity;

        return $this;
    }

    /**
     * Get availableQuantity
     *
     * @return integer
     */
    public function getAvailableQuantity()
    {
        return $this->availableQuantity;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return CartItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     * @return CartItem
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set shippingPrice
     *
     * @param float $shippingPrice
     * @return CartItem
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    /**
     * Get shippingPrice
     *
     * @return float
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    /**
     * Set productSku
     *
     * @param string $productSku
     * @return CartItem
     */
    public function setProductSku($productSku)
    {
        $this->productSku = $productSku;

        return $this;
    }

    /**
     * Get productSku
     *
     * @return string
     */
    public function getProductSku()
    {
        return $this->productSku;
    }

    /**
     * Set productTitle
     *
     * @param string $productTitle
     * @return CartItem
     */
    public function setProductTitle($productTitle)
    {
        $this->productTitle = $productTitle;

        return $this;
    }

    /**
     * Get productTitle
     *
     * @return string
     */
    public function getProductTitle()
    {
        return $this->productTitle;
    }

    /**
     * Transform cart item into an array
     *
     * @return array
     */
    public function toArray()
    {
        $item = array(
            'id' => $this->getId(),
            'offer_id' => $this->getOfferId(),
            'offer_price' => $this->getOfferPrice(),
            'quantity' => $this->getQuantity(),
            'original_quantity' => $this->getOriginalQuantity(),
            'initial_quantity' => $this->getInitialQuantity(),
            'available_quantity' => $this->getAvailableQuantity(),
            'price' => $this->getPrice(),
            'total_price' => $this->getTotalPrice(),
            'shipping_price' => $this->getShippingPrice(),
            'shipping_price_unit' => $this->getShippingPriceUnit(),
            'product_sku' => $this->getProductSku(),
            'product_title' => $this->getProductTitle(),
            'is_drug' => $this->getIsDrug(),
            'ean' => $this->getEan(),
        );

        return $item;
    }

    /**
     * Refresh price of the item
     *
     * @param bool $cleanShipping clean the shipping price
     *
     * @return void
     */
    public function refreshPrice($cleanShipping = false)
    {
        $price = $this->getQuantity() * $this->getOfferPrice();
        $this->setPrice($price);

        if ($cleanShipping) {
            $this->setTotalPrice(null);
            $this->setShippingPrice(null);
        } else {
            $totalPrice = $this->getPrice() + $this->getShippingPrice();
            $this->setTotalPrice($totalPrice);
        }
    }

    /**
     * Is available if the quantity asked is the same as the one given by Mirakl
     *
     * @return bool
     */
    public function isAvailable()
    {
        return ($this->getOriginalQuantity() == $this->getQuantity());
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CartItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CartItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param boolean $isDrug
     */
    public function setIsDrug($isDrug)
    {
        $this->isDrug = $isDrug;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDrug()
    {
        return $this->isDrug;
    }

    /**
     * Set shippingPriceUnit
     *
     * @param float $shippingPriceUnit
     * @return OrderItem
     */
    public function setShippingPriceUnit($shippingPriceUnit)
    {
        $this->shippingPriceUnit = $shippingPriceUnit;

        return $this;
    }

    /**
     * Get shippingPriceUnit
     *
     * @return float
     */
    public function getShippingPriceUnit()
    {
        return $this->shippingPriceUnit;
    }


    /**
     * Replace an item with an other
     *
     * @param CartItem $cartItem
     * @param array    $changedAttributes
     *
     * @throws \Exception
     */
    public function replace($cartItem, array $changedAttributes = null)
    {
        if (! $changedAttributes) {
            $changedAttributes = array(
                'offer_price',
                'offer_id',
                'quantity',
                'original_quantity',
                'price',
                'total_price',
                'shippingPrice',
                'shippingPriceUnit',
                'product_sku',
                'product_title'
            );
        }

        foreach ($changedAttributes as $changedAttributes) {
            // TODO fix crappy regex
            if (preg_match('/^[\a-z0-9]+(_[\a-z0-9])?$/', $changedAttributes)) {
                $formatedAttribute = str_replace(' ', '', ucwords(strtolower(str_replace('_', ' ', $changedAttributes))));
            } elseif (preg_match('/^[\w]+[^ _]$/', $changedAttributes)) {
                $formatedAttribute = ucfirst($changedAttributes);
            } else {
                throw new \Exception(
                    'Attributes array incorrect format. Please use underscore, camelcase, steadycase or constant format',
                    500
                );
            }

            $setMethod = 'set' . $formatedAttribute;
            $getMethod = 'get' . $formatedAttribute;

            if (! method_exists($this, $setMethod) || ! method_exists($this, $getMethod)) {
                throw new \Exception('Call to undefined method '. $setMethod . ' or ' . $getMethod);
            }

            $this->$setMethod($cartItem->$getMethod());
        }

        $this->setOfferId($cartItem->getOfferId());
    }

    public function castAs($itemClass)
    {
        $cartItem = new $itemClass();
        foreach (get_object_vars($this) as $key => $name) {
            $cartItem->$key = $name;
        }

        return $cartItem;
    }
}
