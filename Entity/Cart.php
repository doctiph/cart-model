<?php

namespace Doctipharma\CommonCartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Cart
{
    const UPDATE_STATE_PROMOTION_DELETED = 'PROMOTION_DELETED';

    const OPT_STATE_NOT_LAUNCH = 0;
    const OPT_STATE_PENDING    = 1;
    const OPT_STATE_FOUND      = 2;
    const OPT_STATE_NOT_FOUND  = 3;

    protected $id;

    protected $customerId;

    protected $billingAddressId;

    protected $shippingAddressId;

    protected $price = 0;

    protected $shippingPrice;

    protected $totalPrice = 0;

    protected $totalPriceWithDiscount;

    protected $priceWithDiscount;

    protected $discount;

    protected $discountType;

    protected $discountInformation;

    protected $promotionCode;

    protected $cartShops;

    protected $updatedAt;

    protected $createdAt;

    protected $items = null;

    protected $optimizedCart;

    protected $optimizationState;

    protected $customerHash;

    protected $canBeTransformed;

    protected $hash;

    protected $hasBeenEmailed;

    protected $updateState;

    protected $hasUpdatedQuantities;

    protected $invalidDrugStock;

    /**
     * @var string
     * Comment message link with order
     */

    protected $comment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartShops = new ArrayCollection();
        $this->optimizationState =self::OPT_STATE_NOT_LAUNCH;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id
     *
     * @param $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Add cartShops
     *
     * @param CartShop $cartShops
     * @return Cart
     */
    public function addCartShop(CartShop $cartShops)
    {
        $this->cartShops[] = $cartShops;

        return $this;
    }

    /**
     * Remove cartShops
     *
     * @param CartShop $cartShops
     */
    public function removeCartShop(CartShop $cartShops)
    {
        $this->cartShops->removeElement($cartShops);
    }

    /**
     * Get cartShops
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartShops()
    {
        return $this->cartShops;
    }

    /**
     * Remove $cartItem
     *
     * @param CartItem $cartItem
     */
    public function removeCartItem(CartItem $cartItem)
    {
        foreach ($this->getCartShops() as $shop) {
            foreach ($shop->getCartItems() as $item) {
                if ($cartItem->getId() == $item->getId()) {
                    $shop->removeCartItem($item);
                }
            }

            if ($shop->isEmpty()) {
                $this->removeCartShop($shop);
            }
        }
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Cart
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get list of all items in cart from the different shop
     *
     * @return array
     */
    public function flattenItems()
    {
        if ($this->items) {
            return $this->items;
        }

        $this->items = [];
        foreach ($this->getCartShops() as $shop) {
            foreach ($shop->getCartItems() as $item) {
                $this->items[] = $item;
            }
        }

        return $this->items;
    }

    /**
     * Get list of all items in cart from the different shop
     *
     * @return array
     */
    public function secureFlattenItems()
    {
        $items = array();
        foreach ($this->getCartShops() as $shop) {
            foreach ($shop->getCartItems() as $item) {
                $items[] = $item;
            }
        }

        return $items;
    }

    /**
     * Check if there is items in the cart
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return count($this->flattenItems()) == 0;
    }

    /**
     * get the CartItem by its id
     *
     * @param int $id
     *
     * @return CartItem
     */
    public function getItem($id)
    {
        $item = null;
        foreach ($this->flattenItems() as $cartItem) {
            if ($cartItem->getId() == $id) {
                $item = $cartItem;
                break;
            }
        }

        return $item;
    }


    /**
     * return the CartItem by its offerId
     *
     * @param int $offerId
     *
     * @return CartItem
     */
    public function getItemByOfferId($offerId)
    {
        $item = null;
        foreach ($this->flattenItems() as $cartItem) {
            if ($cartItem->getOfferId() == $offerId) {
                $item = $cartItem;
                break;
            }
        }

        return $item;
    }

    /**
     * Check if at least one item of the cart is not available
     *
     * @return boolean
     */
    public function hasNonAvailableProducts()
    {
        foreach ($this->flattenItems() as $cartItem) {
            if (!$cartItem->isAvailable()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set billingAddressId
     *
     * @param integer $billingAddressId
     * @return Cart
     */
    public function setBillingAddressId($billingAddressId)
    {
        $this->billingAddressId = $billingAddressId;

        return $this;
    }

    /**
     * Get billingAddressId
     *
     * @return integer
     */
    public function getBillingAddressId()
    {
        return $this->billingAddressId;
    }

    /**
     * Set shippingAddressId
     *
     * @param integer $shippingAddressId
     * @return Cart
     */
    public function setShippingAddressId($shippingAddressId)
    {
        $this->shippingAddressId = $shippingAddressId;

        return $this;
    }

    /**
     * Get shippingAddressId
     *
     * @return integer
     */
    public function getShippingAddressId()
    {
        return $this->shippingAddressId;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     * @return Cart
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Get CartShop having a specific shop_id
     *
     * @param int $shopId
     *
     * @return CartShop
     */
    public function getShopByShopId($shopId)
    {
        $shop = null;
        foreach ($this->getCartShops() as $cartShop) {
            if ($cartShop->getShopId() == $shopId) {
                $shop = $cartShop;
                break;
            }
        }

        return $shop;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Cart
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Cart
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getAllProductSkus()
    {
        $productSkus = array();
        foreach ($this->flattenItems() as $item) {
            if (!isset($productSkus[$item->getProductSku()])) {
                $productSkus[$item->getProductSku()] = 0;
            }
            $productSkus[$item->getProductSku()] += $item->getQuantity();
        }

        return $productSkus;
    }

    /**
     * Set shippingPrice
     *
     * @param float $shippingPrice
     * @return CartItem
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    /**
     * Get shippingPrice
     *
     * @return float
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    public function getAllShopIds()
    {
        $shopIds = array();
        foreach ($this->getCartShops() as $shop) {
            $shopIds[] = $shop->getShopId();
        }

        return $shopIds;
    }

    public function getAllOfferIds()
    {
        $ids = [];

        foreach ($this->getCartShops() as $shop) {
            foreach ($shop->getCartItems() as $item) {
                $ids[] = $item->getOfferId();
            }
        }

        return $ids;
    }

    /**
     * @return float
     */
    public function getPriceWithDiscount()
    {
        return $this->priceWithDiscount;
    }

    /**
     * @param float $priceWithDiscount
     */
    public function setPriceWithDiscount($priceWithDiscount)
    {
        $this->priceWithDiscount = $priceWithDiscount;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithDiscount()
    {
        return $this->totalPriceWithDiscount;
    }

    /**
     * @param float $totalPriceWithDiscount
     */
    public function setTotalPriceWithDiscount($totalPriceWithDiscount)
    {
        $this->totalPriceWithDiscount = $totalPriceWithDiscount;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getDiscountType()
    {
        return $this->discountType;
    }

    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;
    }

    public function isShippingDiscount()
    {
        return $this->discountType == "shipping_amount" || $this->discountType == "shipping_percentage";
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
        return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     */
    public function setPromotionCode($promotionCode)
    {
        $this->promotionCode = $promotionCode;
    }

    public function getPriceForNonDrugItems($shopId = null)
    {
        $price = 0;
        foreach ($this->getCartShops() as $shop) {
            if ($shopId != null && $shop->getShopId() != $shopId) {
                continue;
            }

            foreach ($shop->getCartItems() as $item) {
                if (!$item->getIsDrug()) {
                    $price += $item->getPrice();
                }
            }
        }

        return $price;
    }

    public function getPriceForProducts($products)
    {
        $price = 0;
        $products = array_flip($products);
        foreach ($this->getCartShops() as $shop) {
            foreach ($shop->getCartItems() as $item) {
                if (isset($products[$item->getProductSku()])) {
                    $price += $item->getPrice();
                }
            }
        }

        return $price;
    }

    public function countNonDrugItems()
    {
        $quantity = 0;
        foreach ($this->getCartShops() as $shop) {
            foreach ($shop->getCartItems() as $item) {
                if (!$item->getIsDrug()) {
                    $quantity += $item->getQuantity();
                }
            }
        }

        return $quantity;
    }

    /**
     * Set optimizedCart
     *
     * @param OptimizedCart $optimizedCart
     * @return Cart
     */
    public function setOptimizedCart(OptimizedCart $optimizedCart = null)
    {
        $this->optimizedCart = $optimizedCart;

        return $this;
    }

    /**
     * Get optimizedCart
     *
     * @return OptimizedCart
     */
    public function getOptimizedCart()
    {
        return $this->optimizedCart;
    }

    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return integer
     */
    public function getOptimizationState()
    {
        return $this->optimizationState;
    }

    /**
     * @param integer self::OPT_STATE_XXX
     * @return self
     */
    public function setOptimizationState($state)
    {
        $this->optimizationState = $state;
        return $this;
    }

    /**
     * @param string $customerHash
     * @return $this
     */
    public function setCustomerHash($customerHash)
    {
        $this->customerHash = $customerHash;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerHash()
    {
        return $this->customerHash;
    }

    public function setUpdateState($updateState)
    {
        $this->updateState = $updateState;

        return $this;
    }

    public function getUpdateState()
    {
        return $this->updateState;
    }

    /**
     * Set canBeTransformed
     *
     * @param bool $canBeTransformed
     * @return Cart
     */
    public function setCanBeTransformed($canBeTransformed)
    {
        $this->canBeTransformed = $canBeTransformed;

        return $this;
    }

    /**
     * Get canBeTransformed
     *
     * @return bool
     */
    public function getCanBeTransformed()
    {
        return $this->canBeTransformed;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasBeenEmailed()
    {
        return $this->hasBeenEmailed;
    }

    /**
     * @param boolean $hasBeenEmailed
     */
    public function setHasBeenEmailed($hasBeenEmailed)
    {
        $this->hasBeenEmailed = $hasBeenEmailed;
    }

    public function getTotalAmount()
    {
        return $this->totalPriceWithDiscount !== null ? $this->totalPriceWithDiscount : $this->totalPrice;
    }

    public function castAs($cartClass, $optimizedCartClass, $shopClass, $itemClass)
    {
        $cart = new $cartClass();
        foreach (get_object_vars($this) as $key => $name) {
            $cart->$key = $name;
        }

        $cartShops = new ArrayCollection();

        foreach ($this->getCartShops() as $cartShop) {
            $newCartShop = $cartShop->castAs($shopClass, $itemClass);
            $newCartShop->setCart($cart);

            $cartShops[] = $newCartShop;
        }
        $cart->cartShops = $cartShops;

        if ($this->getOptimizedCart()) {
            $optimizedCart = $this->optimizedCart->castAs($optimizedCartClass);
            $optimizedCart->setCart($cart);

            $cart->optimizedCart = $optimizedCart;
        }

        $cart->items = null;

        return $cart;
    }

    public function toCommon()
    {
        return $this->castAs(
            'Doctipharma\\CommonCartBundle\\Entity\\Cart',
            'Doctipharma\\CommonCartBundle\\Entity\\OptimizedCart',
            'Doctipharma\\CommonCartBundle\\Entity\\CartShop',
            'Doctipharma\\CommonCartBundle\\Entity\\CartItem'
        );
    }

    public function hasUpdatedQuantities()
    {
        return $this->hasUpdatedQuantities;
    }

    public function setHasUpdatedQuantities($hasUpdatedQuantities)
    {
        $this->hasUpdatedQuantities = $hasUpdatedQuantities;

        return $this;
    }

    public function getItemsFromShop($shopId)
    {
        $cartShop = $this->getShopByShopId($shopId);

        return $cartShop ? $cartShop->getCartItems() : [];
    }

    public function containsDrugs($shopId = null)
    {
        $cartItems = $shopId != null ? $this->getItemsFromShop($shopId) : $this->flattenItems();

        foreach ($cartItems as $cartItem) {
            if ($cartItem->getIsDrug()) {
                return true;
            }
        }

        return false;
    }

    public function containsOnlyDrugs($shopId = null)
    {
        $cartItems = $shopId != null ? $this->getItemsFromShop($shopId) : $this->flattenItems();

        foreach ($cartItems as $cartItem) {
            if (!$cartItem->getIsDrug()) {
                return false;
            }
        }

        return true;
    }

    public function getShippingModes()
    {
        $modes = [];

        foreach ($this->cartShops as $cartShop) {
            $modes[$cartShop->getShopId()] = $cartShop->getShippingTypeCode();
        }

        return $modes;
    }

    public function hasSelectedShippingMode($mode)
    {
        return in_array($mode, $this->getShippingModes());
    }

    public function getInvalidDrugStock()
    {
        return $this->invalidDrugStock;
    }

    public function setInvalidDrugStock($invalidDrugStock)
    {
        $this->invalidDrugStock = $invalidDrugStock;

        return $this;
    }

    public function getDiscountInformation()
    {
        return $this->discountInformation;
    }

    public function setDiscountInformation($discountInformation)
    {
        $this->discountInformation = $discountInformation;

        return $this;
    }

    /**
     * Gets the Comment message link with order.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets the Comment message link with order.
     *
     * @param string $comment the comment
     *
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }
}
