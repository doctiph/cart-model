<?php

namespace Doctipharma\CommonCartBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Cart bundle extension
 *
 * @author Maxence Dupressoir <mxdup@smile.fr>
 */
class DoctipharmaCommonCartExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $processor     = new Processor();
        $configuration = new Configuration();
        $config        = $processor->processConfiguration($configuration, $configs);

        $def = $container->getDefinition("doctipharma.common_cart.cache");
        $ref = new Reference($config['cache_client']);
        $def->replaceArgument(0, $ref);

        $container->setParameter('doctipharma.common_cart.cache_lifetime', $config['cache_lifetime']);
    }
}
