<?php
namespace Doctipharma\CommonCartBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Cart bundle
 */
class DoctipharmaCommonCartBundle extends Bundle
{
}
